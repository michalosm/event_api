Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      resources :reservations, only: [] do
        post 'pay', on: :member
      end

      resources :tickets, only: [] do
        resources :reservations, only: [:create]
      end

      resources :events, only: [] do
        resources :reservations, only: [:index]
      end

    end
  end

end
