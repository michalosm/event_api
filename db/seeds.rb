5000.times do 
    e = Event.create!(name: Faker::FunnyName.name, time: Faker::Time.forward)
    Ticket.create!(event: e, available: Faker::Number.between(from: 1, to: 100), price: Faker::Number.decimal(l_digits: 3, r_digits: 3))
end
p 'Events and tickets created'