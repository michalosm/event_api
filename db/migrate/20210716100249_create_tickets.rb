class CreateTickets < ActiveRecord::Migration[6.1]
  def change
    create_table :tickets do |t|
      t.integer :available
      t.float :price
      t.references :event, null: false, foreign_key: true
      t.integer :type, default: 0

      t.timestamps
    end
  end
end
