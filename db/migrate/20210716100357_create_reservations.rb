class CreateReservations < ActiveRecord::Migration[6.1]
  def change
    create_table :reservations do |t|
      t.references :ticket, null: false, foreign_key: true
      t.integer :quantity
      t.boolean :confirmed, default: false
      t.timestamps
    end
  end
end
