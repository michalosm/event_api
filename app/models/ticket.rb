class Ticket < ApplicationRecord
  self.inheritance_column = :_type_disabled

  enum type: %i[even avoid_one]

  validates_with Validators::TicketValidator

  belongs_to :event

end
