class Reservation < ApplicationRecord
  validates :quantity, presence: true
  validates_with Validators::ReservationValidator, if: :quantity?

  belongs_to :ticket
  delegate :event, to: :ticket

  def price
    quantity * ticket.price
  end

end
