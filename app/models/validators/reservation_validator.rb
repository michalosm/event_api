module Validators

  class ReservationValidator < ActiveModel::Validator

    def validate(record)
      if record.ticket.even? && record.quantity.odd?
        record.errors.add :quantity, 'We can buy tickets if counts of seats will be even'
      elsif record.ticket.avoid_one? && (record.ticket.available - record.quantity < 2)
        record.errors.add :quantity, 'Tickets sold out'
      end
    end
  end

end
