module Validators
  class TicketValidator < ActiveModel::Validator

    def validate(record)
      if record.avoid_one? and record.available < 2
        record.errors.add :available, 'We can only buy tickets in a quantity that will not leave only 1 ticket available'
      elsif record.available < 0
        record.errors.add :available, 'Tickets sold out'
      end
    end

  end

end
