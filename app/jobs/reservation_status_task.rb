class ReservationStatusTask
  PERIOD_TIME = 15

  def modify_ticket_available_after_period_time(reservation_id)
    reservation = Reservation.find(reservation_id)
    return if reservation.confirmed?

    ticket = reservation.ticket
    ticket.lock!
    ticket.available += reservation.quantity
    ticket.save
  end

  handle_asynchronously :modify_ticket_available_after_period_time, :run_at => Proc.new { PERIOD_TIME.minutes.from_now }

end