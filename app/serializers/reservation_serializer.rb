class ReservationSerializer < ActiveModel::Serializer
  attributes :id, :quantity, :event, :price
end
