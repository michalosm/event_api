class ReservationPayment
  InvalidPaymentTokenError = Class.new(StandardError)

  def self.call(reservation, payment_token)
    raise InvalidPaymentTokenError, 'Invalid token' unless payment_token

    Payment::Gateway.charge(amount: reservation.ticket.price, token: payment_token)
    reservation.update(confirmed: true)
  end

end
