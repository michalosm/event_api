class ReservationBooking
  include Interactor

  NotEnoughTicketsError = Class.new(StandardError)

  def call
    reservation = context.reservation
    reservation.ticket = context.ticket
    if reservation.valid?
      ActiveRecord::Base.transaction do
        ticket = reservation.ticket
        ticket.lock!
        raise NotEnoughTicketsError, 'Not enough tickets left.' if reservation.quantity > ticket.available

        ticket.available -= reservation.quantity.to_i
        ticket.save!
        reservation.save!
        reservation_status_task = ReservationStatusTask.new
        reservation_status_task.modify_ticket_available_after_period_time(reservation.id)
      end
      context.reservation = reservation
    else
      context.fail!(message: 'Invalid reservation')
    end
  end

end