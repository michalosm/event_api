# frozen_string_literal: true
module Api
  module V1
    class ReservationsController < ApiController

      def index
        render json: ReservationRepository.eligible_for_event_id(event, params[:page])
      end

      def create
        reservation_booking = ReservationBooking.call(reservation: Reservation.new(reservation_params), ticket: ticket)
        if reservation_booking.success?
          render json: { status: :ok, reservation: reservation_booking.reservation }
        else
          render json: { error: reservation_booking.reservation.errors }
        end
      end

      def pay
        ReservationPayment.call(reservation, params[:payment_token])
        render json: { success: 'Payment succeeded.' }
      end

      private

      def reservation_params
        params.permit(:quantity)
      end

      def reservation
        @reservation = Reservation.find(params[:id])
      end

      def event
        @event = Event.find(params[:event_id])
      end

      def ticket
        @ticket = Ticket.find(params[:ticket_id])
      end

    end
  end
end
