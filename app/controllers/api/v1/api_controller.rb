# frozen_string_literal: true
module Api
  module V1
    class ApiController < ApplicationController
      rescue_from ActiveRecord::RecordNotFound, with: :not_found_error
      rescue_from ReservationPayment::InvalidPaymentTokenError, with: :payment_failed_error
      rescue_from Payment::Gateway::CardError, with: :payment_failed_error
      rescue_from Payment::Gateway::PaymentError, with: :payment_failed_error

      private

      def not_found_error(error)
        render json: { error: error.message }, status: :not_found
      end

      def payment_failed_error(error)
        render json: { error: error.message }, status: :payment_required
      end

    end
  end
end