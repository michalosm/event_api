class ReservationRepository
  class << self
    def find_by(*attrs)
      Reservation.find_by(*attrs)
    end

    def destroy(reservation)
      reservation.destroy
    end

    def eligible_for_event_id(event_id, number_of_page)
      Reservation.joins(ticket: [:event]).where('events.id=?', event_id).page(number_of_page)
    end
  end
end