FactoryBot.define do
  factory :reservation do
    ticket
    quantity { 2 }
  end
end
  