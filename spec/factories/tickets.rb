FactoryBot.define do
  factory :ticket do
    event
    available { 5 }
    type { 0 }
    price { Faker::Number.decimal(l_digits: 2, r_digits: 2) }
  end

end
