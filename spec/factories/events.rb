FactoryBot.define do
  factory :event do
    name { 'Super event' }
    time { Faker::Time.forward }
    trait :with_ticket do
      after(:create) do |event|
        create(:ticket, event: event)
      end
    end
  end
end