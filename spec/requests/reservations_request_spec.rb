# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'Reservations', type: :request do
  describe 'GET reservations#index' do
    context 'event exists' do

      let(:event) { create(:event, :with_ticket) }
      let(:ticket) { event.ticket }
      let!(:reservation) { create(:reservation, ticket: event.ticket) }
      subject(:index) { get "/api/v1/events/#{event.id}/reservations" }

      before { index }

      context 'reservation exists' do
        it 'should have correct HTTP status' do
          expect(response).to have_http_status(:ok)
        end

        it 'should have correct size' do
          expect(response_json.size).to eq(1)
        end

        it 'should render reservtion with event' do
          expect(response_json[0]).to include(quantity: 2, event: hash_including(id: event.id, name: event.name))
        end

      end
    end

    context 'event not found' do
      subject { get "/api/v1/events/-1/reservations" }
      before { subject }

      it 'should have correct HTTP status' do
        expect(response).to have_http_status(:not_found)
      end

      it 'should render error' do
        expect(response_json).to eq({ error: "Couldn't find Event with 'id'=-1" })
      end

    end

    describe 'POST reservations#create' do
      let(:event) { create(:event, :with_ticket) }
      let(:ticket) { event.ticket }

      context 'correct reservation' do
        subject { post "/api/v1/tickets/#{ticket.id}/reservations", params: { quantity: 2 } }
        before { subject }

        it "creates a new Reservation" do
          expect {
            post "/api/v1/tickets/#{ticket.id}/reservations", params: { quantity: 2 }
          }.to change(Reservation, :count).by(1)
        end

        it 'should have correct HTTP status' do
          expect(response).to have_http_status(:ok)
        end

        it 'should render reservation as response' do
          expect(response_json[:reservation][:quantity]).to eq(2)
          expect(response_json[:reservation][:ticket_id]).to eq(1)
        end

        it 'reservation should be uncorfirmed' do
          expect(response_json[:reservation][:confirmed]).to be_falsey
        end

      end

      context 'incorrect reservation' do
        context 'no quantity params' do
          subject { post "/api/v1/tickets/#{ticket.id}/reservations" }
          before { subject }

          it "doesn't create a new Reservation" do
            expect {
              post "/api/v1/tickets/#{ticket.id}/reservations"
            }.to change(Reservation, :count).by(0)
          end

          it 'should render error as response' do
            expect(response_json).to eq({ :error => { :quantity => ["can't be blank"] } })
          end

        end
      end
    end

    describe 'POST reservations#pay' do
      let(:event) { create(:event, :with_ticket) }
      let(:ticket) { event.ticket }
      let!(:reservation) { create(:reservation, ticket: event.ticket) }

      context 'correct pay with valid payment token' do
        subject(:pay) { post "/api/v1/reservations/#{reservation.id}/pay", params: { 'payment_token': 'example_token' } }

        before { pay }

        it 'should not be reservation confirmed' do
          expect(Reservation.last.confirmed?).to be_truthy
        end

        it 'should render Payment succeeded' do
          expect(response_json[:success]).to eq("Payment succeeded.")
        end

      end

      context 'invalid payment - no payment token' do
        subject(:pay) { post "/api/v1/reservations/#{reservation.id}/pay" }
        before { pay }

        it 'should not be reservation confirmed' do
          expect(Reservation.last.confirmed?).to be_falsey
        end

        it 'should render Invalid token message' do
          expect(response_json).to eq({ :error => 'Invalid token' })
        end

      end

      context 'invalid payment card error' do
        subject(:pay) { post "/api/v1/reservations/#{reservation.id}/pay", params: { 'payment_token': 'card_error' } }
        before { pay }

        it 'should not be reservation confirmed' do
          expect(Reservation.last.confirmed?).to be_falsey
        end

        it 'should render Invalid Payment Card Error' do
          expect(response_json).to eq({ :error => 'Your card has been declined.' })
        end

      end

      context 'invalid payment payment error' do
        subject(:pay) { post "/api/v1/reservations/#{reservation.id}/pay", params: { 'payment_token': 'payment_error' } }
        before { pay }

        it 'should not be reservation confirmed' do
          expect(Reservation.last.confirmed?).to be_falsey
        end

        it 'should render Invalid payment Error' do
          expect(response_json).to eq({ :error => 'Something went wrong with your transaction.' })
        end

      end

    end

  end

  def response_json
    JSON.parse(response.body, symbolize_names: true)
  end
end