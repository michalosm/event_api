# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Reservation, type: :model do
  describe 'Validations' do
    let(:event) { create(:event, :with_ticket) }
    let(:reservation) { create(:reservation, ticket: event.ticket, quantity: 2) }

    context 'New Valid Reservation' do
      it 'should be valid' do
        expect(reservation.valid?).to be true
      end

      it 'should be status unconfirmed' do
        expect(reservation.confirmed?).to be false
      end
    end

    context 'Reservation with Ticket Even Type' do
      let(:ticket_even) { create(:ticket, event: event, available: 4, type: :even) }
      let(:reservation_quantity_even) { build(:reservation, ticket: ticket_even, quantity: 2) }
      let(:reservation_quantity_one) { build(:reservation, ticket: ticket_even, quantity: 1) }

      context 'valid reservation' do
        it 'should be valid reservation' do
          expect(reservation_quantity_even.valid?).to be true
        end
      end

      context 'invalid reservation' do
        it 'should be invalid reservation' do
          expect(reservation_quantity_one.valid?).to be false
        end
      end
    end

    context 'Reservation with Ticket Avoid One' do
      let(:ticket_avoid_one) { create(:ticket, event: event, available: 5, type: :avoid_one) }
      let(:reservation_quantity_two) { build(:reservation, ticket: ticket_avoid_one, quantity: 2) }
      let(:reservation_quantity_four) { build(:reservation, ticket: ticket_avoid_one, quantity: 4) }

      it 'should be valid reservation' do
        expect(reservation_quantity_two.valid?).to be_truthy
      end

      it 'should be invalid reservation' do
        expect(reservation_quantity_four.valid?).to be_falsey
      end
    end
  end
end
