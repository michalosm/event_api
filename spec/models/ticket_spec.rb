require 'rails_helper'

RSpec.describe Ticket, type: :model do
  describe 'Validations' do
    let(:event) { create(:event) }
    let(:ticket) { build(:ticket, event: event, available: 1) }

    context 'Ticket Even' do
      let(:ticket_minus) { build(:ticket, event: event, available: -2) }
      it 'should be possible create available with one quantity' do
        expect(ticket.valid?).to be true
      end

      it 'should not be possible with minus available' do
        expect(ticket_minus.valid?).to be false
      end
    end

    context 'Ticket Avoid One' do
      let (:ticket_avoid_one) { build(:ticket, event: event, available: 1, type: :avoid_one) }
      let (:ticket_avoid_one_three) { build(:ticket, event: event, available: 3, type: :avoid_one) }

      it 'should not be possible create available with one quantity' do
        expect(ticket_avoid_one.valid?).to be false
      end

      it 'should be possible create available with more than one quantity left' do
        expect(ticket_avoid_one_three.valid?).to be true
      end
    end
  end
end
