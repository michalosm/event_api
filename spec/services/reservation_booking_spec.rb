require 'rails_helper'

RSpec.describe ReservationBooking, type: :model do

  describe '#call' do
    let(:event) { create(:event, :with_ticket) }
    let!(:ticket) { event.ticket }
    let(:reservation) { build(:reservation, ticket: event.ticket, quantity: 2) }

    context 'correct reservation' do
      subject(:reservation_booking) { ReservationBooking.call(reservation: reservation,ticket: ticket) }
      before { subject }

      it 'make a reservation and change number of tickets available' do
        expect(reservation_booking.success?).to be true
        expect(ticket.available).to eq(3)
        expect(reservation_booking.reservation).to eq(reservation)
      end

      it 'should run a delayed_job task' do
        expect(Delayed::Job.count).to eq 1
      end

    end

    context 'incorrect reservation' do
      let(:incorrect_reservation) { build(:reservation, ticket: event.ticket, quantity: 6) }

      context 'Not enouugh tickets' do

        it 'shoud NOT make a reservation and not change number of tickets available' do
          expect { ReservationBooking.call(reservation: incorrect_reservation,ticket: ticket) }.to raise_error(ReservationBooking::NotEnoughTicketsError)
          expect(ticket.available).to eq(5)
        end

      end

      context 'Incorrect quantity of reservation' do
        let(:incorrect_quantity) { build(:reservation, ticket: event.ticket, quantity: 3) }

        it 'shoud NOT make a reservation and not change number of tickets available' do
          expect(ReservationBooking.call(reservation: incorrect_quantity,ticket: ticket).success?).to be_falsey
          expect(ticket.available).to eq(5)
        end

      end

    end

  end

end